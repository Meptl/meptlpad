use keyberon::action::{k, l, m, Action, Action::*, HoldTapConfig};
use keyberon::key_code::KeyCode::*;

/* Keyberon was made for key switch matrices, our keyboard doesn't follow this wiring layout, so we
 * map it using this macro. Unfortunately we can't have the comma-less array definitions that
 * keyberon has because we aren't using procedural macros.
 *
 * Note that NumLock/$d/$dd is hard coded. This is because '{' and '(' are special macro
 * characters.
 */
macro_rules! mux_map {
    ( {
        [$a:ident, $b:ident, $c:ident, $d:ident],
        [$e:ident, $f:ident, $g:ident, $h:ident],
        [$i:ident, $j:ident, $k:ident, $l:ident],
        [$m:ident, $n:ident, $o:ident, $p:ident],
      }
      {
        [$aa:ident, $bb:ident, $cc:ident, $dd:ident],
        [$ee:ident, $ff:ident, $gg:ident, $hh:ident],
        [$ii:ident, $jj:ident, $kk:ident, $ll:ident],
        [$mm:ident, $nn:ident, $oo:ident, $pp:ident],
      }
    ) => {
        keyberon::layout::layout! {
            {
                // Should be $d
                [$n $j $m $i],
                [$e $a $f $b],
                [$c $g $d $h],
                [$l $p $k $o],
                [{l(1)} t t t],
            }
            {
                // Should be $dd
                [$nn $jj $mm $ii],
                [$ee $aa $ff $bb],
                [$cc $gg $dd $hh],
                [$ll $pp $kk $oo],
                [{l(1)} t t t],
            }
        }
    };
}

#[rustfmt::skip]
pub static LAYERS: keyberon::layout::Layers<4, 5, 2, ()> = mux_map! {
    {
        [Kp1, Kp2, Kp3, NumLock],
        [Kp4, Kp5, Kp6, KpAsterisk],
        [Kp7, Kp8, Kp9, KpPlus],
        [BSpace, Kp0, KpDot, KpEnter],
    }
    {
        [Home, Up, End, NumLock],
        [Left, Down, Right, KpSlash],
        [PgDown, Down, PgUp, KpMinus],
        [Delete, Space, KpEqual, KpEnter],
    }
};
