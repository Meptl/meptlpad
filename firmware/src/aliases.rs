use analog_multiplexer::{DummyPin, Multiplexer};
use stm32f4xx_hal::gpio::gpioa::{PA0, PA1, PA15, PA2, PA3, PA4, PA5};
use stm32f4xx_hal::gpio::gpiob::{PB12, PB8};
use stm32f4xx_hal::gpio::gpioc::PC14;
use stm32f4xx_hal::gpio::{Analog, Output, PushPull};

pub type S0 = PC14<Output<PushPull>>;
pub type S1 = PB8<Output<PushPull>>;
pub type S2 = PB12<Output<PushPull>>;
pub type S3 = PA15<Output<PushPull>>;
pub type EN = DummyPin; // We assume the enable pin goes to GND at all times

pub type SelectPins = (S0, S1, S2, S3, EN);
pub type Multiplex = Multiplexer<SelectPins>;

pub type AnalogPins = (
    PA0<Analog>,
    PA1<Analog>,
    PA2<Analog>,
    PA3<Analog>,
    PA4<Analog>,
);
