#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(clippy::empty_loop)]
#![no_main]
#![no_std]
#![feature(type_alias_impl_trait)]

// Halt on panic
use panic_halt as _; // panic handler

use analog_multiplexer::{DummyPin, Multiplexer};
use core::ops::Div;
use keyberon::key_code::KbHidReport;
use keyberon::layout::{Event, Layout};
use stm32f4xx_hal::adc::{config::AdcConfig, config::Clock, config::SampleTime, Adc};
use stm32f4xx_hal::gpio::gpioa::PA0;
use stm32f4xx_hal::gpio::gpioc::PC13;
use stm32f4xx_hal::gpio::{Analog, Output, PushPull};
use stm32f4xx_hal::otg_fs::{UsbBusType, USB};
use stm32f4xx_hal::pac::{ADC1, TIM2};
use stm32f4xx_hal::prelude::*;
use stm32f4xx_hal::timer::MonoTimerUs;
use usb_device::bus::UsbBusAllocator;
use usb_device::prelude::*;
use usbd_dfu_rt::{DfuRuntimeClass, DfuRuntimeOps};

use keyberon::action::Action;
use keyberon::hid::HidDevice;

mod aliases;
mod layout;
mod multiplexers;

const MV_DIVISOR: u16 = 4; // A cheap way to deal with voltage wobble/average things out
                           // FYI: Setting the divisor to 4 should give us 80-100 "steps" of resolution per sensor
const NORTH_DOWN: bool = false;
const ACTUATION_THRESHOLD: u16 = 20;
const RELEASE_THRESHOLD: u16 = 5;
const IGNORE_BELOW: u16 = 60;

pub struct Leds {
    num_lock: PC13<Output<PushPull>>,
    pub status: bool,
}
impl keyberon::keyboard::Leds for Leds {
    fn num_lock(&mut self, status: bool) {
        if status {
            self.num_lock.set_low();
        } else {
            self.num_lock.set_high();
        }
        self.status = status;
    }
}

#[rtic::app(device = stm32f4xx_hal::pac, peripherals = true, dispatchers = [USART1])]
mod app {
    use super::*;
    use usb_device::class::UsbClass; // Needed for keyboard.poll() and mouse.poll() to work

    #[shared]
    struct Shared {
        usb_dev: usb_device::device::UsbDevice<'static, UsbBusType>,
        usb_class: keyberon::Class<'static, UsbBusType, Leds>,
        dfu: DfuRuntimeClass<DFUBootloader>,
    }

    #[local]
    struct Local {
        multiplexer: aliases::Multiplex,
        adc: Adc<ADC1>,
        mx_com: PA0<Analog>,
        mx_com_states: multiplexers::ChannelStates,
        layout: Layout<4, 5, 2, ()>,
        num_lock_state: Option<bool>,
    }

    #[monotonic(binds = TIM2, default = true)]
    type MicrosecMono = MonoTimerUs<TIM2>;

    #[init(local = [
        USB_BUS: Option<UsbBusAllocator<UsbBusType>> = None,
        EP_MEMORY: [u32; 1024] = [0; 1024]
    ])]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        // Setup GPIO.
        let gpioa = cx.device.GPIOA.split();
        let gpiob = cx.device.GPIOB.split();
        let gpioc = cx.device.GPIOC.split();

        // Setup clocks
        let rcc = cx.device.RCC.constrain();
        let clocks = rcc
            .cfgr
            .use_hse(25.MHz())
            .sysclk(84.MHz())
            .require_pll48clk()
            .freeze();

        // Configure TIM2 as clock source.
        let mono = cx.device.TIM2.monotonic(&clocks);

        // Pclk2_div_2 gives us an ADC running at 42Mhz (because sysclk is at 84Mhz)
        let adc_config = AdcConfig::default().clock(Clock::Pclk2_div_2);
        // NOTE: With a 42Mhz ADC clock and a SampleTime of Cycles_480 we'd be able
        // to read all 16 channels on all six analog multiplexers (96 pins) in about
        // 1.25ms which is just *barely* too slow for a 1000Hz USB polling rate.
        // For this reason we take the next cycle down at Cycles_28 which results in
        // a read-all-channels time of about 0.160ms. So that's the best accuracy
        // we can get without going over our 1ms budget.
        // NOTE: Formula for calcuating number of cycles: ((<cycles>+12)/42)*96
        let mut adc = Adc::adc1(cx.device.ADC1, true, adc_config);
        let mx_com = gpioa.pa0.into_analog();
        let mut mx_com_states: multiplexers::ChannelStates = Default::default();
        let s0 = gpioc.pc14.into_push_pull_output();
        let s1 = gpiob.pb8.into_push_pull_output();
        let s2 = gpiob.pb12.into_push_pull_output();
        let s3 = gpioa.pa15.into_push_pull_output();
        let en = DummyPin; // Just run it to GND to keep always-enabled

        let mut multiplexer = Multiplexer::new((s0, s1, s2, s3, en));

        // Read in the initial millivolt values for all analog channels so we have
        // a default/resting state to evaluate against.  We'll set new defaults later
        // after we've captured a few values (controlled by DEFAULT_WAIT_MS).
        for chan in 0..16 {
            // This sets the channel on all multiplexers simultaneously
            // (since they're all connected to the same S0,S1,S2,S3 pins).
            multiplexer.set_channel(chan);
            let sample = adc.convert(&mx_com, SampleTime::Cycles_28);
            let millivolts = adc.sample_to_millivolts(sample).div(MV_DIVISOR);
            mx_com_states[chan.into()].update_default(millivolts);
        }

        let led = gpioc.pc13.into_push_pull_output();
        let leds = Leds {
            num_lock: led,
            status: false,
        };

        // BlackPill board has a pull-up resistor on the D+ line.
        // Pull the D+ pin down to send a RESET condition to the USB bus.
        // This forced reset is needed only for development, without it host
        // will not reset your device when you upload new firmware.
        let mut usb_dp = gpioa.pa12.into_push_pull_output();
        usb_dp.set_low();
        cortex_m::asm::delay(1024 * 10);

        let usb = USB {
            usb_global: cx.device.OTG_FS_GLOBAL,
            usb_device: cx.device.OTG_FS_DEVICE,
            usb_pwrclk: cx.device.OTG_FS_PWRCLK,
            pin_dm: stm32f4xx_hal::gpio::alt::otg_fs::Dm::PA11(gpioa.pa11.into_alternate()),
            pin_dp: stm32f4xx_hal::gpio::alt::otg_fs::Dp::PA12(usb_dp.into_alternate()),
            hclk: clocks.hclk(),
        };
        *cx.local.USB_BUS = Some(UsbBusType::new(usb, cx.local.EP_MEMORY));
        let usb_bus = cx.local.USB_BUS.as_ref().unwrap();
        let dfu = DfuRuntimeClass::new(cx.local.USB_BUS.as_ref().unwrap(), DFUBootloader);

        let usb_class = keyberon::new_class(usb_bus, leds);
        let usb_dev = keyberon::new_device(usb_bus);

        tick::spawn_after(1.millis()).ok();

        (
            Shared {
                usb_dev,
                usb_class,
                dfu,
            },
            Local {
                multiplexer,
                adc,
                layout: Layout::new(&crate::layout::LAYERS),
                mx_com,
                mx_com_states,
                num_lock_state: None,
            },
            init::Monotonics(mono),
        )
    }

    #[task(binds = OTG_FS, priority = 3, shared = [usb_dev, usb_class, dfu])]
    fn usb_tx(cx: usb_tx::Context) {
        (cx.shared.usb_dev, cx.shared.usb_class, cx.shared.dfu).lock(|usb_dev, usb_class, dfu| {
            if usb_dev.poll(&mut [usb_class, dfu]) {
                usb_class.poll();
            }
        });
    }

    #[task(binds = OTG_FS_WKUP, priority = 3, shared = [usb_dev, usb_class, dfu])]
    fn usb_rx(cx: usb_rx::Context) {
        (cx.shared.usb_dev, cx.shared.usb_class, cx.shared.dfu).lock(|usb_dev, usb_class, dfu| {
            if usb_dev.poll(&mut [usb_class, dfu]) {
                usb_class.poll();
            }
        });
    }

    #[task(priority = 2,
        shared = [ usb_class, dfu ],
        local = [ multiplexer, adc, mx_com, mx_com_states, layout, num_lock_state ]
    )]
    fn tick(mut cx: tick::Context) {
        tick::spawn_after(1.millis()).ok();

        let adc = cx.local.adc;
        let multiplexer = cx.local.multiplexer;
        let layout = cx.local.layout;

        match layout.tick() {
            _ => (),
        }

        cx.shared.dfu.lock(|dfu| dfu.tick(100));

        // Read in the initial millivolt values for all analog channels so we have
        // a default/resting state to evaluate against.  We'll set new defaults later
        // after we've captured a few values (controlled by DEFAULT_WAIT_MS).
        for chan in 0..16 {
            // This sets the channel on all multiplexers simultaneously
            // (since they're all connected to the same S0,S1,S2,S3 pins).
            multiplexer.set_channel(chan);
            let mut ch_state = cx.local.mx_com_states[chan.into()];
            let sample = adc.convert(cx.local.mx_com, SampleTime::Cycles_28);
            let millivolts = adc.sample_to_millivolts(sample).div(MV_DIVISOR);
            ch_state.record_value(millivolts);
            if ch_state.value <= IGNORE_BELOW {
                continue;
            }

            let voltage_difference = if millivolts < ch_state.default {
                if NORTH_DOWN {
                    ch_state.default - millivolts // North side down switches result in a mV drop
                } else {
                    0
                }
            } else {
                if NORTH_DOWN {
                    0
                } else {
                    millivolts - ch_state.default // South side down switches result in a mV increase
                }
            };

            if voltage_difference > ACTUATION_THRESHOLD {
                if !ch_state.pressed {
                    cx.local.mx_com_states.press(chan.into());
                    let _ = layout.event(Event::Press(chan / 4, chan % 4));
                }
            } else if voltage_difference < RELEASE_THRESHOLD {
                if ch_state.pressed {
                    cx.local.mx_com_states.release(chan.into());
                    let _ = layout.event(Event::Release(chan / 4, chan % 4));
                }
            }
        }

        // Grab canonical num lock state
        let mut num_lock = false;
        cx.shared
            .usb_class
            .lock(|k| num_lock = k.device_mut().leds_mut().status);
        // Simulate the layer switch button as needed. This assumes the num-lock enabled layer is
        // not the default.
        if cx.local.num_lock_state.is_some() {
            if cx.local.num_lock_state.unwrap() != num_lock {
                *cx.local.num_lock_state = Some(num_lock);
                if num_lock {
                    let _ = layout.event(Event::Release(4, 0));
                } else {
                    let _ = layout.event(Event::Press(4, 0));
                }
            }
        } else {
            // First run.
            if !num_lock {
                let _ = layout.event(Event::Press(4, 0));
            }
            *cx.local.num_lock_state = Some(num_lock);
        }

        // Send usb keyboard report
        let report: KbHidReport = layout.keycodes().collect();
        if cx
            .shared
            .usb_class
            .lock(|k| k.device_mut().set_keyboard_report(report.clone()))
        {
            while let Ok(0) = cx.shared.usb_class.lock(|k| k.write(report.as_bytes())) {}
        }
    }

    pub struct DFUBootloader;

    const KEY_STAY_IN_BOOT: u32 = 0xb0d42b89;

    impl DfuRuntimeOps for DFUBootloader {
        const DETACH_TIMEOUT_MS: u16 = 500;
        const CAN_UPLOAD: bool = false;
        const WILL_DETACH: bool = true;

        fn detach(&mut self) {
            cortex_m::interrupt::disable();

            let cortex = unsafe { cortex_m::Peripherals::steal() };

            let p = 0x2000_0000 as *mut u32;
            unsafe { p.write_volatile(KEY_STAY_IN_BOOT) };

            cortex_m::asm::dsb();
            unsafe {
                // System reset request
                cortex.SCB.aircr.modify(|v| 0x05FA_0004 | (v & 0x700));
            }
            cortex_m::asm::dsb();
            loop {}
        }
    }
}
