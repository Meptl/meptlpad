# Firmware
Keyboard firmware for a 4x4 numpad.

Configured to run on a WeAct Black Pill.
I used an Black Magic Probe to flash the board, but the firmware also supports
flashing via dfu.

# Configuring/Building
You may need to configure NORTH_DOWN in src/main.rs depending on the orientation
of the magnets in the case.

# Flashing

`cargo run` flashes the firmware via a debugger connected to the Black Pill's
SWDP pings.
See `.config/cargo.toml`

For dfu-util flashing, prepare the compiled binary for the microcontroller:
```
arm-none-eabi-objcopy -Obinary target/thumbv7em-none-eabi/debug/meptlpad meptlpad.bin
```

Hold the BOOT0 button while pressing the RESET button.
Use `dfu-util --list` to find the "Internal Flash" alt (likely 0).
If you have other dfu devices present also record the serial.

Then flash the device:
```
dfu-util -S '206A34C43931' -a 0  -D meptlpad.bin -s 0x08000000
```
