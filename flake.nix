{
  inputs = {
      nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
      meptlpkgs.url = "gitlab:Meptl/meptlpkgs";

      fenix = {
        url = "github:nix-community/fenix";
        inputs.nixpkgs.follows = "nixpkgs";
      };
  };

  outputs = { self, fenix, nixpkgs, meptlpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfree = true;
    };
  in
  {
    devShell.${system} = pkgs.mkShell {
      buildInputs = with pkgs; [
        pre-commit

        (fenix.packages.${system}.fromToolchainFile {
          file = ./rust-toolchain.toml;
          sha256 = "sha256-0Zg9ScHUFM3/pSTG39JEH/OH19PN+b1aRsLbaYBsQ1Y=";
        })
        gcc-arm-embedded

        kicad

        dfu-util

        meptlpkgs.packages.${system}.plasticity
      ];
    };
  };
}
