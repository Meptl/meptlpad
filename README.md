# Meptlpad
A 4x4 keypad featuring a print-in-place magnetic key switch integrated into the top case.

![](./showcase.png)

An implementation of [riskeyboard70](https://github.com/riskable/riskeyboard70).

# Setup
This repository uses git-lfs.
```
git lfs pull
```
This repository uses git submodules.
```
git submodule update --init
```
