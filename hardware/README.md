# Hardware

![](./hardware.png)

The following hardware pieces are used:
- [WeAct BlackPill (STM32F411CEU6)](https://www.aliexpress.us/item/3256801269871873.html)
- [Right Angled Type C Connector](https://www.aliexpress.us/item/2255800399055426.html)
- [4x2mm magnets](https://www.amazon.com/gp/product/B08NZS7WBK)
- [Lubricant](https://www.amazon.com/dp/B0081JE0OO)
- [Neoprene](https://www.amazon.com/dp/B018CW1W22)

It's useful to have:
- [Micro flush cutters](https://www.amazon.com/dp/B00FZPDG1K)
- Some flat magnetic surface such as the tip of pliers e.g. [tip of this](https://www.amazon.com/dp/B003RWS8OI)
- Super glue (Because my prints are imperfect)

# Keycaps
For keycaps, I used the
[pregenerated model for the riskeyboard](https://github.com/riskable/keycap_playground)

## Right Angled Type C Connector Specifications
![](./right-angled-c-specs.webp)
