# Case and Switch models
![](./actuate.gif)

## Assembly
1. Remove the following pieces highlighted in red:

![](./images/removable.png)

   In the full case these pieces have been extended vertically.
   For the bottom (of the switch) side, the notches align with where you need to cut.

1. Release and actuate the switch. You can use this section of the switch as a
   lever to pull on the switch.

![](./images/releaselever.png)

1. Clean up the actuation. Preferrably until the switch naturally actuates with
   gravity. Some may get stuck at the extremes of the actuation, but I've found
   lubrication to remedy this. Consider grabbing the side with the cherry cross
   with pliers to aid in lapping.

1. Insert case magnet. If filling the full case, ensure all magnets are the same
   polarity.

![](./images/casemagnet.png)

   The hooked tip of micro flush cutters are very helpful here.
   Alternatively, you can use the upper magnet holder as a flat surface to
   push the magnet, but apply pressure evenly as it is thin and can break.

1. Insert lower magnet. You can use the magnetic attraction of the already
   inserted case magnet to orient this one.

   ![](./images/lowermagnet.png)

1. Insert upper magnet. This should repel the case magnet.

   ![](./images/uppermagnet.png)

   I usually allow the magnet to naturally fall into place then touch it with pliers.
   The magnet will attach to the pliers, then I can flip it and use the same pliers to set
   the magnet into the holder.

If any magnet fails to stay in place, a spot of super glue may be needed.
I've found maybe 2 out of 48 magnet holders of a case needing adhesive,
although my filaments are _always_ wet.

## Development
I used a trial of Plasticity 1.2.18 to develop these model files.

Plasticity doesn't have any form of referential objects, so changes in
switch.plasticity need to be migrated to switcharray.plasticity and then
case.plasticity.

### switch.plasticity
A single key switch body. Use this for development.

### switcharray.plasticity
switch.plasticity duplicated 4x4 times. The distance between each switch body is
19.05mm.
To generate a new switcharray from the single switch body:
* Without deleting the vertical railings, delete all old switch bodies
* Import the new switch.array
* Array duplicate the switch along the XZ axis with a distance of 19.05*3 and
    repeat of 4
* Extrude a "new body" to fill the space between two switches
* Array duplicate the new extrusion to tie join all the switches

### case.plasticity
The top and bottom case.
To generate a new top case from the switch array:
* Import the new switcharray.plasticity (You may have to delete some buggy solids)
* Select the new imported objects and group them (Ctrl-G)
* Move the switcharray onto the existing switcharray. Use the Freestyle move
    command to snap each corner.
* Select the old switcharray (Alt-click the group)
* Delete the old switcharray

### switch-legacy.plasticity
An old switch with a different print orientation.
Doesn't work with FDM printers because of layer lines.
Possibly works with resin printers, so I've kept it.
