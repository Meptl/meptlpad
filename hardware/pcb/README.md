# Meptlpad PCB

Derived from [riskable's reference pcb](https://github.com/riskable/void_switch_65_pct)

# Exports
The exports folder contains my export for JLCPCB.
pcb.zip is the gerber/drill files.
bom.csv excludes the multiplexer; I manually soldered that on (it costs $3 to add that item)

# Building
## JLCPCB
See [JLCPCB's guide](https://jlcpcb.com/help/article/81-How-to-generate-the-BOM-and-Centroid-file-from-KiCAD).
I also used this [kicad plugin](https://gist.github.com/arturo182/a8c4a4b96907cfccf616a1edb59d0389#file-bom2grouped_csv_jlcpcb-xsl)
at one point but I don't recall if I needed.

### BOM
I manually deleted a bunch of unwanted/unused components.

### Pick and Place file
There are a few manual edits needed within the pick and place pos.csv file.
- The rotation for some components are 180 degrees out of phase compared to
JLCPCB's machine when generated from kicad.
- As described by JLCPCB, the pos.csv headers are
`Designator,Val,Package,Mid X,Mid Y,Rotation,Layer` compared to whatever kicad
outputs.
